<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Form\MdpType;
use App\Form\ModifyType;
use App\Service\UploadService;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index()
    {
        return $this->render('user/index.html.twig');
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        return $this->render('accueil/logout.html.twig');
    }
    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $errors = $authenticationUtils->getLastAuthenticationError();
        $username = $authenticationUtils->getLastUsername();
        return $this->render('accueil/login.html.twig', [
            'errors' => $errors,
            'username' => $username
        ]);
    }

    /**
     * @Route("/compte", name="compte")
     */
    public function MonCompte()
    {
        return $this->render('user/compte.html.twig');
    }

    /**
     * @Route("/register/{user}", name="register")
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $encoder,
        ObjectManager $manager,
        UploadService $service,
        User $user = null
    ) {
        dump($this->getUser());

        if (!$user) {
            $user = new User();
        } else {
            if ($user != $this->getUser()) {
                return new Response('You don\'t have permission to modify this profile', 401);
            }
        }
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * Lorsqu'on enregistre un user, il faut encoder son mot de
             * passe en utilisant le UserPasswordEncoderInterface
             */
            $pass = $encoder->encodePassword($user, $user->getPassword());
            //Puis remplacer son mot de passe en clair par le mot de passe hashé
            $user->setPassword($pass);
            if ($user->getRoles()[0] === 'ROLE_ADMIN') {
                $user->setRoles('ROLE_ADMIN');
            } else {
                $user->setRoles('ROLE_USER');
            }
            if ($user->getFileImage()) {
                $user->setImage($service->upload($user->getFileImage()));
                $user->setFileImage(null);
            }

            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('accueil');
        }

        return $this->render('form/register.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/modify/{user}", name="modify")
     */
    public function modify(
        Request $request,
        UserPasswordEncoderInterface $encoder,
        ObjectManager $manager,
        UploadService $service,
        User $user
    ) {
        dump($this->getUser());

        if ($user != $this->getUser()) {
            return new Response('You don\'t have permission to modify this profile', 401);
        }
        $form = $this->createForm(ModifyType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * Lorsqu'on enregistre un user, il faut encoder son mot de
             * passe en utilisant le UserPasswordEncoderInterface
             */
            //Puis remplacer son mot de passe en clair par le mot de passe hashé
            if ($user->getRoles()[0] === 'ROLE_ADMIN') {
                $user->setRoles('ROLE_ADMIN');
            } else {
                $user->setRoles('ROLE_USER');
            }
            if ($user->getFileImage()) {
                $user->setImage($service->upload($user->getFileImage()));
                $user->setFileImage(null);
            }

            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('compte');
        }

        return $this->render('form/register.html.twig', [
            'form' => $form->createView()

        ]);
    }

    /**
     * @Route("/modifyMdp/{user}", name="modifyMdp")
     */
    public function modifyMdp(
        Request $request,
        UserPasswordEncoderInterface $encoder,
        ObjectManager $manager,
        UploadService $service,
        User $user
    ) {
        dump($this->getUser());

        if ($user != $this->getUser()) {
            return new Response('You don\'t have permission to modify this profile', 401);
        }
        $form = $this->createForm(MdpType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $pass = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($pass);

            if ($user->getRoles()[0] === 'ROLE_ADMIN') {
                $user->setRoles('ROLE_ADMIN');
            } else {
                $user->setRoles('ROLE_USER');
            }
            if ($user->getFileImage()) {
                $user->setImage($service->upload($user->getFileImage()));
                $user->setFileImage(null);
            }

            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('compte');
        }

        return $this->render('form/register.html.twig', [
            'form' => $form->createView()

        ]);
    }
}
